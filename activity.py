# S02 Activity:
# 1. Get a year from the user and determine if it is a leap year or not.
# 2.  Get two numbers (row and col) from the user and create by row x col grid of asterisks given the two numbers.
# Stretch goal: Implement error checking for the leap year input
# 1. Strings are not allowed for inputs
# 2. No zero or negative values

#1

test = int(input("Please enter desired year: "))
if test <= 0:
    print(f"Year {test} is not a valid year.")
elif test % 4 == 0:
    print(f"Year {test} is a leap year.")
else:
    print(f"Year {test} is not a leap year.")       
#2

test2 = int(input("Please enter rows: "))
test3 = int(input("Please enter columns: "))

# for x in range(test2):
#     for y in range(test3):
#         print("*", end=" ")
#     print()
for x in range(test2):
    asterisk = " "
    for y in range(test3):
        asterisk += "*"
    print(asterisk)    
